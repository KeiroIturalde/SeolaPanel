# SeolaPanel

SeolaPanel is a simple virtualization control panel written in Python with Flask. SeolaPanel utilizes the libvirt API to interact directly with the hypervisor, and thus (will eventually) support all virtualization technology that libvirt supports.

This project is still in a relatively early stage of development, it is feature complete but there is still a fair amount of work to be done. With that being said, use at your own risk!

The project's official website is: [https://seola.tech](https://seola.tech)

---

## Current Version - 0.0

Initial commit.

## Please Note:

Nothing to note, for now.™

---

## Help Support SeolaPanel

---

## Requirements

* Hardware virtualization.
* Centos 6 / Debian -- implies Ubuntu support by default.
* Python3
* MongoDB
* KVM (currently only support virtualization technology)
* RabbitMQ (used by Celery)
* Variety of Python packages shown [here](https://github.com/KeiroD/SeolaPanel/blob/master/requirements.txt)

---

## Installation

1. Install git client `yum install git` for CentOS 'apt-get install git' for Debian/Ubuntu. 
2. `mkdir /srv/seola && cd /srv/seola && git clone https://github.com/KeiroD/seola-panel.git .`
3. Configure `virbr0` as a bridge interface, [this](http://www.linux-kvm.org/page/Networking) may be useful in accomplishing this.
4. `./scripts/setup.sh` - This will install all dependencies, start necessary services and start Space.
5. Navigate to `...` to complete the setup process.
6. Make the directories for your disks, images and configs, by default these are `/var/disks`, `/var/images`, `/var/configs`. Make them with `mkdir /var/disks /var/images /var/configs` if you are using the defaults. 
7. Add an image to the `/var/images` directory, you can use wget to do that. 
8. Login via `...`
9. Go to `Networking` and add an IP range. 
10. Go to `...` to import your new image.
11. Make your first virtual machine and enjoy!

---

## Troubleshooting

1. I see command `example` was not found

  This means pip failed to install stuff, first make sure the `pip` command works. If it doesn't, install python-pip using yum `yum install python-pip` or easy_install `easy_install pip`. After pip is installed, install the requirements `pip -r /srv/seola/requirements.txt`.
  
2. Experiencing general weirdness, things not installing, etc.

Other strangeness is usually attributed to not running SeolaPanel as `root`, make sure you are using the root user when installing SeolaPanel, starting/stopping seola, etc.

3. DHCPD failed to start

This is expected (sort of). You should see DHCPD start normally after you add an IP range. If that doesn't happen, `dnsmasq` is likely running on that port. Kill it with `pkill dnsmasq` and then try `service dhcpd start`.

4. Other stuff

SeolaPanel is new and I don't have a ton of people to test it, so there are <strong>certainly</strong> problems I'm not aware of. If you encounter one, please open an issue and I'll take a look.

---

## Notes & Misc. 

* The password for the console will not be shown when you type (or paste), this is expected, it will still work. 
* If you are brave and try this and things break, please let me know so I can fix them :) 
